import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import statsmodels.api as sm
from matplotlib.font_manager import FontProperties
from scipy.stats import norm
from sklearn import metrics
from sklearn import svm
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve
from sklearn.model_selection import cross_val_predict
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.tree import DecisionTreeClassifier


def getNormProb(minval,maxval,mean,stdev):
    loc=mean
    scale=stdev
    bins=maxval-minval+1
    x = np.linspace(minval, maxval, bins)
    out=norm.pdf(x, loc, scale)
    surplus=(1-out.sum())/(maxval-minval+1)
    p=[]
    for val in out:
        val2=val+surplus
        p.append(val2)
    return p

def getDiscreteValuesNormalized(minvalue,maxvalue,mean,stdev,samples):
    values=[]
    for i in np.arange(0,samples):
        value=np.random.choice(np.arange(minvalue, maxvalue+1), p=getNormProb(minvalue,maxvalue,mean,stdev))
        values.append(value)
    return values

def generate_dataset(minval,maxval,meanpos,stdevpos,meanneg,stdevneg,possamples,negsamples,colnum):
    pos=[]
    for i in np.arange(0,possamples):
        posrow=[]
        for j in np.arange(0,colnum):
            posval=np.random.choice(np.arange(minval, maxval+1), p=getNormProb(minval,maxval,meanpos,stdevpos))
            posrow.append(posval)
        posrow.append(1)
        pos.append(posrow)
    neg=[]
    for i in np.arange(0,negsamples):
        negrow=[]
        for j in np.arange(0,colnum):
            negval=np.random.choice(np.arange(minval, maxval+1), p=getNormProb(minval,maxval,meanneg,stdevneg))
            negrow.append(negval)
        negrow.append(0)
        neg.append(negrow)

    neg.extend(pos)
    cols=[]
    for k in np.arange(1,colnum+1):
        cols.append('x'+str(k))
    cols.append('y')
    return pd.DataFrame(neg, columns=cols)


def generate_dataset_parametric(k,ratio_side,stdev,population,pos,neg,features):
    minval=1
    maxval=k
    meanpos= (k+1)/2 + ratio_side*((k+1)/2-1)
    meanneg=(k+1)/2 - ratio_side*((k+1)/2-1)
    stdevpos=stdev
    stdevneg=stdev
    possamples=population*pos/(pos+neg)
    negsamples=population*neg/(pos+neg)
    frame=generate_dataset(minval,maxval,meanpos,stdevpos,meanneg,stdevneg,possamples,negsamples,features)
    return frame


def evaluate_cross_fold(df,folds,classifier):
    predicted = cross_val_predict(classifier, df.drop('y', axis=1), df.y, cv=folds)
    tn, fp, fn, tp = metrics.confusion_matrix(df.y, predicted).ravel()
    acc=(tp+tn)/(tn+fp+fn+tp)
    auc=roc_auc_score(df.y, predicted,'weighted')
    tpr=tp/(tp+fn)
    tnr=tn/(fp+tn)
    ppv=tp/(tp+fp)
    npv=tn/(fn+tn)
    return acc,auc,tpr,tnr,ppv,npv



def thresholdValueBinary(a,thres, pos_label,neg_label):
    if (a>thres): return pos_label
    else: return neg_label

def getConfusionMatrixfromPredictedScores(y_test, predicted_scores):
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    if not np.isnan(predicted_scores).any():
        try:
            fpr, tpr, thresholds = roc_curve(y_test, predicted_scores, pos_label=1)
        except ValueError:
            print('value error on thresholds')
    tnr_tpr=np.ones(len(fpr))-fpr+tpr # Sum of True Positive Rate and 1 minus False Positive Rate
    imax=np.argmax(tnr_tpr)
    best_thres=thresholds[imax]
    vecfunc = np.vectorize(thresholdValueBinary)
    predicted = vecfunc(predicted_scores,best_thres,1,0)
    tn, fp, fn, tp = metrics.confusion_matrix(y_test, predicted).ravel()
    return tn, fp, fn, tp, predicted

def composite_indices_model(x_train, x_test, y_train, y_test):
#    %%%%% Binary Logistic Regression (BLR) %%%%%%
    results=[]
    glm_binom = sm.GLM(sm.add_constant(y_train), sm.add_constant(x_train),
                       family=sm.families.Binomial(link=sm.genmod.families.links.logit))
    glm_result = glm_binom.fit()
    BLR=glm_result.params
    _, features=x_train.shape
    coef_m=[]
    coef_u=np.zeros(features)
    DEV_u=np.zeros(features)
    coef_ds_u=np.zeros(features)
    coef_ds_m=np.zeros(features)
    coef_m[1:features]=BLR[1:features+1]
    coef_m=np.exp(coef_m) # Odds Ratio from Multivariate Binary Logistic Regression
    DEV_m=glm_result.deviance
    for k in np.arange(features): # Loop for the m components  - k is the variable that runs from 1 until kmax
        glm_binom = sm.GLM(sm.add_constant(y_train), sm.add_constant(x_train.values[:,k]),family=sm.families.Binomial(link=sm.genmod.families.links.logit))
        glm_result = glm_binom.fit()
        BLR=glm_result.params
        coef_u[k]=np.exp(BLR[1]) # Odds Ratio from Univariate Binary Logistic Regression
        DEV_u[k]=glm_result.deviance #  Deviance from Univariate Binary Logistic Regression
        coef_ds_u[k]=coef_u[k]/DEV_u[k] # Odds Ratio from Univariate Binary Logistic Regression divided by Deviance
        coef_ds_m[k]=coef_m[k]/DEV_u[k] # Odds Ratio from Multivariate Binary Logistic Regression divided by Deviance
    sumcoef_u=sum(coef_u)/features # Sum of UBLR OR divided by kmax
    w_ulr=coef_u/sumcoef_u # Weights from UBLR OR
    sumcoef_m=sum(coef_m)/features # Sum of MBLR OR divided by kmax
    w_mlr=coef_m/sumcoef_m # Weights from MBLR OR
    sumcoef_ds_u=sum(coef_ds_u)/features # Sum of (UBLR OR divided by Deviance Statistic) divided by kmax
    w_ulr_ds=coef_ds_u/sumcoef_ds_u # Weights from UBLR OR/DS
    sumcoef_ds_m=sum(coef_ds_m)/features  #Sum of (MBLR OR divided by Deviance Statistic) divided by kmax
    w_mlr_ds=coef_ds_m/sumcoef_ds_m # Weights from MBLR OR/DS

    w=np.zeros((features,5))# construction of weight matrix
    w[:,0]=np.ones((1,features))
#     ww[:,0,ix]=w[:,0]# first column ones ;
    w[:,1]=w_ulr
#     ww[:,1,ix]=w[:,1] # second column: UBLR weights
    w[:,2]=w_mlr
#     ww[:,2,ix]=w[:,2] # third column: MBLR weights
    w[:,3]=w_ulr_ds
#     ww[:,3,ix]=w[:,3] # fourth column: UBLR/DS weights
    w[:,4]=w_mlr_ds
#     ww[:,4,ix]=w[:,4] # fifth column: UBLR/DS weights

    T=np.zeros((len(y_train),5), dtype=float) # Initialazation of indices' matrix - Assigning dimensions (n1+n2)*9
    T=np.dot(x_test,w) #  Multiplication of matrices X and w in order to produce the indices T0-T4
    for i in np.arange(5):
        predicted_scores=T[:,i]  # the predictions for each composite index
        tn, fp, fn, tp, predicted = getConfusionMatrixfromPredictedScores(y_test, predicted_scores)
        acc=(tp+tn)/(tn+fp+fn+tp)
        auc=roc_auc_score(y_test, predicted, 'weighted')
        if tp+fn >0:
            tpr=tp/(tp+fn)
        else:
            tpr=0
        if fp+tn >0:
            tnr=tn/(fp+tn)
        else:
            tnr=0
        if tp+fp>0:
            ppv=tp/(tp+fp)
        else:
            ppv=0
        if fn+tn >0:
            npv=tn/(fn+tn)
        else:
            npv=0
        row=[]
        row.extend((acc,auc,tpr,tnr,ppv,npv))
        results.append(row)
    return results



def plotpair(names,colors,means,stds,labels):
    N = 2
    ind = np.arange(N)  # the x locations for the groups
    width = 0.1       # the width of the bars
    fig, ax = plt.subplots()
    for i in np.arange(len(names)):
        rects = ax.bar(ind+i*width, means[i], width, color=colors[i], yerr=stds[i])

    # add some text for labels, title and axes ticks
    ax.set_ylabel('Scores')
    ax.set_title('Scores by algorithm')
    ax.set_xticks(ind + width / 2)
    ax.set_xticklabels(labels)
    fontP = FontProperties()
    fontP.set_size('small')
    ax.legend(names,ncol=6,bbox_to_anchor=(1.04,0.0,0.2,1), loc=4,prop=fontP,borderaxespad=0, mode=None)

    plt.tight_layout(rect=[0,0,0.8,1])
    plt.show()

def plotsingle(names,colors,means,stds,labels):
    N = 1
    ind = np.arange(N)  # the x locations for the groups
    width = 0.18       # the width of the bars
    font = {'family': 'serif',
            'weight': 'bold',
            'size': 10}
    plt.rc('font', **font)
    fig, ax = plt.subplots()
    for i in np.arange(len(names)):
        rects = ax.bar(ind+i*width, means[i], width, color=colors[i], yerr=stds[i])

    # add some text for labels, title and axes ticks
    ax.set_ylabel('Scores')
    ax.set_title('Scores by algorithm')
    ax.set_xticks(ind + width / N)
    ax.set_xticklabels(labels)
    fontP = FontProperties()
    fontP.set_size('x-small')
    ax.legend(names,ncol=1, loc='best',prop=fontP,borderaxespad=0, mode=None)
    plt.show()



def evaluate_algorithms(df,folds,algos):
    results=[]
    names=[]
    for name,model in algos:
        acc,auc,tpr,tnr,ppv,npv=evaluate_cross_fold(df,folds,model)
        row=[]
        row.extend((acc,auc,tpr,tnr,ppv,npv))
        results.append(row)
        names.append(name)
    return names,results


def evaluate_train_test_split(df,models,test_ratio):
    results=[]
    for name,m in models:
        x_train, x_test, y_train, y_test = train_test_split(df.drop('y', axis=1), df.y, test_size=test_ratio, stratify=df.y)
        model = m.fit(x_train, y_train)
        ymat=y_test.values
        predicted = model.predict(x_test)
        auc=roc_auc_score(ymat, predicted,'weighted')
        tn, fp, fn, tp = metrics.confusion_matrix(y_test, predicted).ravel()
        acc=(tp+tn)/(tn+fp+fn+tp)
        if tp+fn >0:
            tpr=tp/(tp+fn)
        else:
            tpr=0
        if fp+tn >0:
            tnr=tn/(fp+tn)
        else:
            tnr=0
        if tp+fp>0:
            ppv=tp/(tp+fp)
        else:
            ppv=0
        if fn+tn >0:
            npv=tn/(fn+tn)
        else:
            npv=0
        row=[]
        row.extend((acc,auc,tpr,tnr,ppv,npv))
        results.append(row)
    comboresults=composite_indices_model(x_train, x_test, y_train, y_test)
    results=results+comboresults[1:]
    return results

def load_models_names_colors():
    models=[]
    lr=LogisticRegression()
    dt=DecisionTreeClassifier()
    svc=svm.SVC(probability=True)
    mlp=MLPClassifier(max_iter=2000)
    nb=GaussianNB()
    knn=KNeighborsClassifier(n_neighbors=5)
    rf=RandomForestClassifier()
    models.append(('LR', lr))
    models.append(('DT', dt))
    models.append(('SVM', svc))
    models.append(('NNet', mlp))
    models.append(('NB', nb))
    models.append(('Knn', knn))
    models.append(('RF', rf))
    models.append(('GB',GradientBoostingClassifier(n_estimators=300, learning_rate=1.0,max_depth=1, random_state=0)))
    names=['LR','DT','SVM','NNet','NB','Knn','RF','GB','ULR','MLR','ULRDS','MLRDS']
    colors = ['yellow', 'yellowgreen', 'green', 'cyan', 'lightblue','blue','darkgreen', 'darkblue','orange','darkorange','pink','red']
    return models,names,colors



def plot_metric(means,std,metric,plot_xlabel,value, xvalues):
    x=xvalues
    if metric=='acc':
        m=0
        plot_title='Accuracy'
        plot_ylabel = 'Accuracy'
    elif metric=='auc':
        m=1
        plot_title = 'Area Under Curve (AUC)'
        plot_ylabel = 'AUC'
    elif metric == 'tpr':
        m =2
        plot_title = 'Sensitivity (TPR)'
        plot_ylabel = 'TPR'
    ylr=[]
    errorlr=[]
    ydt = []
    errordt = []
    ysvm=[]
    errorsvm=[]
    ynb=[]
    errornb=[]
    ygb = []
    errorgb = []
    ymlr=[]
    errormlr=[]
    ymlrds=[]
    errormlrds=[]
    for i in np.arange(len(means)):
        ylr.append(means[i][0,m])
        errorlr.append(means[i][0, m])
        ydt.append(means[i][1, m])
        errordt.append(means[i][1, m])
        ysvm.append(means[i][2, m])
        errorsvm.append(means[i][2, m])
        ynb.append(means[i][4, m])
        errornb.append(means[i][4, m])
        ygb.append(means[i][7, m])
        errorgb.append(means[i][7, m])
        ymlr.append(means[i][10, m])
        errormlr.append(means[i][10, m])
        ymlrds.append(means[i][11, m])
        errormlrds.append(means[i][11, m])

    plt.figure(figsize=(20,10))
    font = {'family' : 'serif',
            'weight' : 'bold',
            'size'   : 22}
    plt.rc('font', **font)
    plt.plot(x, ylr, 'k+-',linewidth=4, markersize=12, color='goldenrod')
    # plt.fill_between(x, ylr-errorlr, ylr+errorlr, color='yellow', alpha=0.7)
    plt.plot(x, ydt, 'ko-',linewidth=4, markersize=12, color='yellowgreen')
    # plt.fill_between(x, ydt-errordt, ydt+errordt, color='yellowgreen', alpha=0.7)
    plt.plot(x, ysvm, 'k*-.',linewidth=4, markersize=12, color='green')
    # plt.fill_between(x, ysvm-errorsvm, ysvm+errorsvm, color='green', alpha=0.7)
    plt.plot(x, ynb, 'k,-.',linewidth=4, markersize=12, color='lightblue')
    # plt.fill_between(x, ynb-errornb, ynb+errornb, color='lightblue', alpha=0.7)
    plt.plot(x, ygb, 'k^--',linewidth=4, markersize=12,color='darkblue')
    # plt.fill_between(x, ygb-errorgb, ygb+errorgb, color='darkblue', alpha=0.7)
    plt.plot(x, ymlrds, 'k^--',linewidth=4, markersize=12,color='red')
    # plt.fill_between(x, ymlrds-errormlrds, ymlrds+errormlrds, color='red', alpha=0.7)
    fontP = FontProperties()
    fontP.set_size('large')
    plt.title(plot_title, fontsize=38)
    plt.xlabel(plot_xlabel, fontsize=38)
    plt.ylabel(plot_ylabel, fontsize=38)
    plt.xticks(x, fontsize = 36)
    plt.yticks(fontsize = 36)
    plt.legend(['LR','DT','SVM','NB','GB','MLRDS'],ncol=1, loc='lower right',prop=fontP,borderaxespad=0, mode=None)
    # plt.show()
    plt.savefig('figures/'+metric+'_'+value+'.eps', format='eps', dpi=600)



def autolabel(rects, ax):
    # Get y-axis height to calculate label position from.
    (y_bottom, y_top) = ax.get_ylim()
    y_height = y_top - y_bottom

    for rect in rects:
        height = rect.get_height()

        # Fraction of axis height taken up by this rectangle
        p_height = (height / y_height)

        # If we can fit the label above the column, do that;
        # otherwise, put it inside the column.
        if p_height > 0.95: # arbitrary; 95% looked good to me.
            label_position = height - (y_height * 0.05)
        else:
            label_position = height + (y_height * 0.01)
        barlab=ax.text(rect.get_x() + rect.get_width()/2., label_position,'%.2f' % height,ha='center', va='bottom')
        barlab.set_fontsize(20)
