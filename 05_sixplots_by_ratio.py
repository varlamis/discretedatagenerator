from utils import *

def pipeline_montecarlo(k,ratio_side,stdev,population,pos,neg,features, test_ratio,mc_iter,action):
    if action=='load':
        df=pd.read_csv('datasets/exp5_k='+str(k)+'_pop='+str(population)+'_feat='+str(features)+'_1:'+str(neg)+'.csv',sep=',', encoding='utf-8')
        df=df.drop(df.columns[0], axis = 1) 

    elif action=='save':
        df=generate_dataset_parametric(k,ratio_side,stdev,population,pos,neg,features)
        df.to_csv('datasets/exp5_k='+str(k)+'_pop='+str(population)+'_feat='+str(features)+'_1:'+str(neg)+'.csv', sep=',', encoding='utf-8')
        return
    # prepare models
    models,names,colors = load_models_names_colors()

    r=evaluate_algorithms_montecarlo(df,models,test_ratio,mc_iter)
    means=np.mean(r,axis = 0)
    std=np.std(r,axis = 0)
    return means,std


def evaluate_algorithms_montecarlo(df,algos,test_ratio,iterations):
    allresults=[]
    for iter in np.arange(iterations):
        results=evaluate_train_test_split(df,algos,test_ratio)
        allresults.append(results)
    return np.array(allresults)




def generate_and_save_datasets():
    pipeline_montecarlo(5,0.2,1,1000,1,1,10,0.3,100,'save')
    pipeline_montecarlo(5,0.2,1,1000,1,2,10,0.3,100,'save')
    pipeline_montecarlo(5,0.2,1,1000,1,4,10,0.3,100,'save')
    pipeline_montecarlo(5,0.2,1,1000,1,9,10,0.3,100,'save')
    pipeline_montecarlo(5,0.2,1,1000,5,95,10,0.3,100,'save')


def load_datasets():
    means1, std1 = pipeline_montecarlo(5, 0.2, 1, 1000, 1, 1, 10, 0.3, 100, 'load')
    means2, std2 = pipeline_montecarlo(5, 0.2, 1, 1000, 1, 2, 10, 0.3, 100, 'load')
    means4, std4 = pipeline_montecarlo(5, 0.2, 1, 1000, 1, 4, 10, 0.3, 100, 'load')
    means9, std9 = pipeline_montecarlo(5, 0.2, 1, 1000, 1, 9, 10, 0.3, 100, 'load')
    means95, std95 = pipeline_montecarlo(5, 0.2, 1, 1000, 5, 95, 10, 0.3, 100, 'load')
    return [means1,means2,means4,means9,means95],[std1,std2,std4,std9,std95]


def main():
    generate_and_save_datasets()
    x = [1, 2, 4, 9, 95]
    means, std = load_datasets()
    plot_metric(means, std, 'auc', 'Ratio pos:neg (1:x)', 'ratio', x)
    plot_metric(means, std, 'tpr', 'Ratio pos:neg (1:x)', 'ratio', x)


if __name__ == "__main__":
    main()
    print("done")

