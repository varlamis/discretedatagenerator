from utils import *


def pipeline_montecarlo(k,ratio_side,stdev,population,pos,neg,features, test_ratio,mc_iter,action):
    if action=='load':
        df=pd.read_csv('datasets/exp6_k='+str(k)+'_pop='+str(population)+'_feat='+str(features)+'_lambda=:'+str(ratio_side)+'.csv',sep=',', encoding='utf-8')
        df=df.drop(df.columns[0], axis = 1)
    elif action=='save':
        df=generate_dataset_parametric(k,ratio_side,stdev,population,pos,neg,features)
        df.to_csv('datasets/exp6_k='+str(k)+'_pop='+str(population)+'_feat='+str(features)+'_lambda=:'+str(ratio_side)
                  +'.csv', sep=',', encoding='utf-8')
        return
    # prepare models
    models,names,colors = load_models_names_colors()

    r=evaluate_algorithms_montecarlo(df,models,test_ratio,mc_iter)
    means=np.mean(r,axis = 0)
    std=np.std(r,axis = 0)
    return means,std


def evaluate_algorithms_montecarlo(df,algos,test_ratio,iterations):
    allresults=[]
    for iter in np.arange(iterations):
        results=evaluate_train_test_split(df,algos,test_ratio)
        allresults.append(results)
    return np.array(allresults)


def generate_and_save_datasets():
    pipeline_montecarlo(5, 0.1, 1, 1000, 1, 4, 10, 0.3, 100, 'save')
    pipeline_montecarlo(5, 0.2, 1, 1000, 1, 4, 10, 0.3, 100, 'save')
    pipeline_montecarlo(5, 0.5, 1, 1000, 1, 4, 10, 0.3, 100, 'save')
    pipeline_montecarlo(5, 1, 1, 1000, 1, 4, 10, 0.3, 100, 'save')


def load_datasets():
    means1, std1 = pipeline_montecarlo(5, 0.1, 1, 1000, 1, 4, 10, 0.3, 100, 'load')
    means2, std2 = pipeline_montecarlo(5, 0.2, 1, 1000, 1, 4, 10, 0.3, 100, 'load')
    means5, std5 = pipeline_montecarlo(5, 0.5, 1, 1000, 1, 4, 10, 0.3, 100, 'load')
    means10, std10 = pipeline_montecarlo(5, 1, 1, 1000, 1, 4, 10, 0.3, 100, 'load')
    return [means1,means2,means5,means10],[std1,std2,std5,std10]



def main():
    generate_and_save_datasets()
    x = [0.1, 0.2, 0.5, 1]
    means, std = load_datasets()
    plot_metric(means, std, 'auc', 'λ', 'lambda', x)
    plot_metric(means, std, 'tpr', 'λ', 'lambda', x)


if __name__ == "__main__":
    main()
    print("done")
