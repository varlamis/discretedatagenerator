from utils import *

def pipeline_montecarlo(k,ratio_side,stdev,population,pos,neg,features, test_ratio,mc_iter,action):
    if action=='load':
        df=pd.read_csv('datasets/exp7_k=5_pop='+str(population)+'_feat='+str(features)+'_lambda=:'+str(ratio_side)+'.csv',sep=',', encoding='utf-8')
        df=df.drop(df.columns[0], axis = 1)
    elif action=='save':
        df=generate_dataset_parametric(k,ratio_side,stdev,population,pos,neg,features)
        df.to_csv('datasets/exp7_k='+str(k)+'_pop='+str(population)+'_feat='+str(features)+'_lambda=:'+str(ratio_side)
                  +'.csv', sep=',', encoding='utf-8')
        return
    # prepare models
    models,names,colors = load_models_names_colors()

    r=evaluate_algorithms_montecarlo(df,models,test_ratio,mc_iter)
    means=np.mean(r,axis = 0)
    std=np.std(r,axis = 0)
    return means,std


def evaluate_algorithms_montecarlo(df,algos,test_ratio,iterations):
    allresults=[]
    for iter in np.arange(iterations):
        if iter%10==0:
            print(iter)
        results=evaluate_train_test_split(df,algos,test_ratio)
        allresults.append(results)
    return np.array(allresults)



def discretize(df,k):
    df_discrete=df.copy(deep=True)
    for i in np.arange(len(df_discrete.columns)-1):
        col=df_discrete[df_discrete.columns[i]]
        df_discrete.iloc[:, i] = pd.cut(col, k, labels=False)
    return df_discrete

def pipeline_montecarlo_actual_data(k,test_ratio,mc_iter,df_discrete):
    # prepare models
    models,names,colors = load_models_names_colors()

    r=evaluate_algorithms_montecarlo(df_discrete,models,test_ratio,mc_iter)
    means=np.mean(r,axis = 0)
    std=np.std(r,axis = 0)
    # plotsingle(names,colors,means[:,0:1],std[:,0:1],('acc',''))
    # plotsingle(names,colors,means[:,1:2],std[:,1:2],('auc',''))
    # plotsingle(names,colors,means[:,2:3],std[:,2:3],('tpr',''))
    # plotsingle(names,colors,means[:,3:4],std[:,3:4],('tnr',''))
    # plotsingle(names,colors,means[:,4:5],std[:,4:5],('ppv',''))
    # plotsingle(names,colors,means[:,5:6],std[:,5:6],('npv',''))
    return means,std


def process_single_partition(df,k,iter):
    return pipeline_montecarlo_actual_data(k, 0.3, iter, df)


def process(df):
    means3, std3 = pipeline_montecarlo_actual_data(3, 0.3, 100, df)
    means5, std5 = pipeline_montecarlo_actual_data(5, 0.3, 100, df)
    means7, std7 = pipeline_montecarlo_actual_data(7, 0.3, 100, df)
    means9, std9 = pipeline_montecarlo_actual_data(9, 0.3, 100, df)
    means11, std11 = pipeline_montecarlo_actual_data(11, 0.3, 100, df)
    return [means3,means5,means7,means9, means11],[std3,std5,std7,std9, std11]


def standardizeFeature(column):
    colmean=column.mean()
    coldev=column.std()
    column=(column-colmean)/coldev
    return column


def standardize(df_raw):
    df_standard=df_raw.copy(deep=True)
    for i in np.arange(len(df_standard.columns)-1):
        col=df_standard[df_standard.columns[i]]
        df_standard.iloc[:, i] = standardizeFeature(col)
    return df_standard

def plotsingle_attika(names,colors,means,stds,labels, title, yaxis, axes, locx, locy):
    # plt.rcParams['font.size']= 10
    ax=axes[locx, locy]
    ax.set_ylim([0, 1.2])

    N = len(names)
    ind = np.arange(0,N)  # the x locations for the groups
    a=axes[locx, locy].bar(ind,tuple(means.ravel()[0:N]),width=0.6,yerr=tuple(stds.ravel()[0:N]))

    ax.set_ylabel(yaxis,fontsize=30)
    ax.set_xlabel('Classifier',fontsize=30)
    ax.set_title(title, fontsize = 38)
    ax.set_xticks(ind)
    ax.set_xticklabels(names,rotation=45,fontsize = 26)

    for i in np.arange(0,N):
        a[i].set_facecolor(colors[i])
    autolabel(a,ax)

def get_scores_discretize_only(raw,k,iter):
    df_discrete=discretize(raw,k)
    return process_single_partition(df_discrete,k,iter)


def get_scores_standardize_and_discretize(raw,k,iter):
    df_standard=standardize(raw)
    df_discrete=discretize(df_standard,k)
    return process_single_partition(df_discrete,k, iter)


def main():
    df_raw = pd.read_csv("datasets/ATTICA_Study_10_yr_for_CVD_DISCRETE.csv")
    df_raw=df_raw.drop(df_raw.columns[-4:], axis = 1)
    df_raw.columns.values[len(df_raw.columns)-1] = 'y'
    k=7
    iter=100
    # means, std = get_scores_discretize_only(df_raw,k,iter)
    means, std = get_scores_standardize_and_discretize(df_raw,k,iter)

    plt.rcParams['font.size']=26
    fig, axes = plt.subplots(3, 2)
    fig.set_figwidth(25)
    fig.set_figheight(20)
    fig.set_dpi(200)
    models, names, colors = load_models_names_colors()
    # fig.figure(figsize=(20,10))
    plotsingle_attika(names,colors,means[:,0:1],std[:,0:1],('acc',''),'Accuracy','Acc',axes,0,0)
    plotsingle_attika(names,colors,means[:,1:2],std[:,1:2],('auc',''),'Area Under Curve','AUC',axes,0,1)
    plotsingle_attika(names,colors,means[:,2:3],std[:,2:3],('tpr',''),'True Positive Rate','TPR',axes,1,0)
    plotsingle_attika(names,colors,means[:,3:4],std[:,3:4],('tnr',''),'True Negative Rate','TNR',axes,1,1)
    plotsingle_attika(names,colors,means[:,4:5],std[:,4:5],('ppv',''),'Positive Predictive Value','PPV',axes,2,0)
    plotsingle_attika(names,colors,means[:,5:6],std[:,5:6],('npv',''),'Negative Predictive Value','NPV',axes,2,1)
    fig.tight_layout()
    plt.savefig('figures/algorithms_attica.eps', format='eps', dpi=600)

def main2():
    df_raw = pd.read_csv("datasets/ATTICA_Study_10_yr_for_CVD_DISCRETE.csv")
    df_raw = df_raw.drop(df_raw.columns[-4:], axis=1)
    df_raw.columns.values[len(df_raw.columns) - 1] = 'y'
    x = [5, 7, 9, 11, 15, 25, 51]
    iter = 20
    means=[]
    stds=[]
    for i in np.arange(len(x)):
        mean,std= get_scores_standardize_and_discretize(df_raw, x[i], iter)
        # means, std = get_scores_discretize_only(df_raw,k,iter)
        means.append(mean)
        stds.append(std)

    plot_metric(means, std, 'auc', 'Number of partitions (k)', 'attika_by_k', x)
    plot_metric(means, std, 'tpr', 'Number of partitions (k)', 'attika_by_k', x)




if __name__ == "__main__":
    main2()
    print("done")
