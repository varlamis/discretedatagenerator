from utils import *


def pipeline_montecarlo(k,ratio_side,stdev,population,pos,neg,features, test_ratio,mc_iter, action):
    if action=='load':
        df=pd.read_csv('datasets/exp1_k='+str(k)+'_pop='+str(population)+'_feat='+str(features)+'.csv',sep=',', encoding='utf-8')
        df=df.drop(df.columns[0], axis = 1) 
    elif action=='save':
        df=generate_dataset_parametric(k,ratio_side,stdev,population,pos,neg,features)
        df.to_csv('datasets/exp1_k='+str(k)+'_pop='+str(population)+'_feat='+str(features)+'.csv', sep=',', encoding='utf-8')
        return
    
    # prepare models
    models , names, colors= load_models_names_colors()

    r=evaluate_algorithms_montecarlo(df,models,test_ratio,mc_iter)
    means=np.mean(r,axis = 0)
    std=np.std(r,axis = 0)
    # plotsingle(names,colors,means[:,0:1],std[:,0:1],('acc',''))
    # plotsingle(names,colors,means[:,1:2],std[:,1:2],('auc',''))
    # plotsingle(names,colors,means[:,2:3],std[:,2:3],('tpr',''))
    # plotsingle(names,colors,means[:,3:4],std[:,3:4],('tnr',''))
    # plotsingle(names,colors,means[:,4:5],std[:,4:5],('ppv',''))
    # plotsingle(names,colors,means[:,5:6],std[:,5:6],('npv',''))
    return means,std


def evaluate_algorithms_montecarlo(df,algos,test_ratio,iterations):
    allresults=[]
    for iter in np.arange(iterations):
        results=evaluate_train_test_split(df,algos,test_ratio)
        allresults.append(results)
    return np.array(allresults)


def plot_value_distribution():
    df=pd.read_csv('datasets/exp1_k=5_pop=1000_feat=10.csv',sep=',', encoding='utf-8')
    df=df.drop(df.columns[0], axis = 1)

    dfpos=df[df.y==1]
    dfneg=df[df.y==0]
    plt.figure(figsize=(20,10))
    font = {'family' : 'serif',
            'weight' : 'bold',
            'size'   : 22}
    plt.rc('font', **font)
    plt.hist([dfneg.x1,dfpos.x1], 5, density=True, color=['r','g'], alpha=1)
    fontP = FontProperties()
    fontP.set_size('large')
    plt.title('Value distribution', fontsize=38)
    plt.xlabel('Value', fontsize=38)
    plt.ylabel('Percentage', fontsize=38)
    plt.xticks([1,2,3,4,5], fontsize = 36)
    plt.yticks(fontsize = 36)
    plt.legend(['Negative','Positive'],ncol=1, loc='best',prop=fontP,borderaxespad=0, mode=None)
    plt.savefig('figures/value_distribution.eps', format='eps', dpi=600)


def plotsingle_new(names,colors,means,stds,labels, title, yaxis, axes, locx, locy):
    ax=axes[locx, locy]
    ax.set_ylim([0, 1.2])

    N = len(names)
    ind = np.arange(0,N)  # the x locations for the groups
    a=axes[locx, locy].bar(ind,tuple(means.ravel()[0:N]),width=0.6,yerr=tuple(stds.ravel()[0:N]))
    ax.set_ylabel(yaxis,fontsize=30)
    ax.set_xlabel('Classifier',fontsize=30)
    ax.set_title(title, fontsize = 38)
    ax.set_xticks(ind)
    ax.set_xticklabels(names,rotation=45,fontsize = 26)

    for i in np.arange(0,N):
        a[i].set_facecolor(colors[i])
    autolabel(a,ax)

def plot_algorithms():
    means,std=pipeline_montecarlo(5,0.2,1,1000,1,4,10,0.3,100,'load')
    names=['LR','DT','SVM','NNet','NB','Knn','RF','GB','ULR','MLR','ULRDS','MLRDS']
    colors = ['yellow', 'yellowgreen', 'green', 'cyan', 'lightblue','blue','darkgreen', 'darkblue','orange','darkorange','pink','red']
    plt.rcParams['font.size']=26
    fig, axes = plt.subplots(3, 2)
    fig.set_figwidth(25)
    fig.set_figheight(20)
    fig.set_dpi(200)
    plotsingle_new(names,colors,means[:,0:1],std[:,0:1],('acc',''),'Accuracy','Acc',axes,0,0)
    plotsingle_new(names,colors,means[:,1:2],std[:,1:2],('auc',''),'Area Under Curve','AUC',axes,0,1)
    plotsingle_new(names,colors,means[:,2:3],std[:,2:3],('tpr',''),'True Positive Rate','TPR',axes,1,0)
    plotsingle_new(names,colors,means[:,3:4],std[:,3:4],('tnr',''),'True Negative Rate','TNR',axes,1,1)
    plotsingle_new(names,colors,means[:,4:5],std[:,4:5],('ppv',''),'Positive Predictive Value','PPV',axes,2,0)
    plotsingle_new(names,colors,means[:,5:6],std[:,5:6],('npv',''),'Negative Predictive Value','NPV',axes,2,1)
    fig.tight_layout()
    plt.savefig('figures/algorithms.eps', format='eps', dpi=600)


def main():
    # pipeline_montecarlo(5,0.2,1,1000,1,4,10,0.3,100,'save')
    plot_value_distribution()
    plot_algorithms()

if __name__ == "__main__":

    main()
    print("done")
