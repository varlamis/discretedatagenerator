
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import statsmodels.api as sm
from matplotlib.font_manager import FontProperties
from scipy.stats import norm
from sklearn import metrics
from sklearn import svm
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve
from sklearn.model_selection import cross_val_predict
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.tree import DecisionTreeClassifier
from utils import *

def pipeline_montecarlo(k,ratio_side,stdev,population,pos,neg,features, maxfeatures, test_ratio,mc_iter,action):
    if action=='load':
        df=pd.read_csv('datasets/exp3_k='+str(k)+'_pop='+str(population)+'_feat='+str(maxfeatures)+'.csv',sep=',', encoding='utf-8')
        df=df.drop(df.columns[0], axis = 1) 
        df=df.iloc[:,maxfeatures-features:]
    elif action=='save':
        df=generate_dataset_parametric(k,ratio_side,stdev,population,pos,neg,maxfeatures)
        df.to_csv('datasets/exp3_k='+str(k)+'_pop='+str(population)+'_feat='+str(maxfeatures)+'.csv', sep=',', encoding='utf-8')
        return
    # prepare models
    models,names,colors = load_models_names_colors()

    r=evaluate_algorithms_montecarlo(df,models,test_ratio,mc_iter)
    means=np.mean(r,axis = 0)
    std=np.std(r,axis = 0)
    return means,std


def evaluate_algorithms_montecarlo(df,algos,test_ratio,iterations):
    allresults=[]
    for iter in np.arange(iterations):
        if iter%25==0:
            print(iter)
        results=evaluate_train_test_split(df,algos,test_ratio)
        allresults.append(results)
    return np.array(allresults)


def generate_and_save_datasets():
    pipeline_montecarlo(5,0.2,1,1000,1,4,50,50,0.3,100,'save')



def load_datasets():
    means50, std50 = pipeline_montecarlo(5, 0.2, 1, 1000, 1, 4, 50, 50, 0.3, 100, 'load')
    means20, std20 = pipeline_montecarlo(5, 0.2, 1, 1000, 1, 4, 20, 50, 0.3, 100, 'load')
    means15, std15 = pipeline_montecarlo(5, 0.2, 1, 1000, 1, 4, 15, 50, 0.3, 100, 'load')
    means10, std10 = pipeline_montecarlo(5, 0.2, 1, 1000, 1, 4, 10, 50, 0.3, 100, 'load')
    means5, std5 = pipeline_montecarlo(5, 0.2, 1, 1000, 1, 4, 5, 50, 0.3, 100, 'load')
    return [means5,means10,means15,means20,means50],[std5,std10,std15,std20,std50]


def main():
    generate_and_save_datasets()
    x = [5, 10, 15, 20, 50]
    means, std = load_datasets()
    plot_metric(means, std, 'auc', 'Features (feat)', 'features', x)
    plot_metric(means, std, 'tpr', 'Features (feat)', 'features', x)


if __name__ == "__main__":
    main()
    print("done")


