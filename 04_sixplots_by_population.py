from utils import *

def pipeline_montecarlo(k,ratio_side,stdev,population,pos,neg,features, test_ratio,mc_iter,action):
    if action=='load':
        df=pd.read_csv('datasets/exp4_k=5_pop='+str(population)+'_feat='+str(features)+'.csv',sep=',', encoding='utf-8')
        df=df.drop(df.columns[0], axis = 1) 
    elif action=='save':
        df=generate_dataset_parametric(k,ratio_side,stdev,population,pos,neg,features)
        df.to_csv('datasets/exp4_k='+str(k)+'_pop='+str(population)+'_feat='+str(features)+'.csv', sep=',', encoding='utf-8')
        return
    # prepare models
    models,names,colors = load_models_names_colors()

    r=evaluate_algorithms_montecarlo(df,models,test_ratio,mc_iter)
    means=np.mean(r,axis = 0)
    std=np.std(r,axis = 0)
    return means,std


def evaluate_algorithms_montecarlo(df,algos,test_ratio,iterations):
    allresults=[]
    for iter in np.arange(iterations):
        results=evaluate_train_test_split(df,algos,test_ratio)
        allresults.append(results)
    return np.array(allresults)


def generate_and_save_datasets():
    pipeline_montecarlo(5,0.2,1,100,1,4,10,0.3,100,'save')
    pipeline_montecarlo(5,0.2,1,1000,1,4,10,0.3,100,'save')
    pipeline_montecarlo(5,0.2,1,10000,1,4,10,0.3,100,'save')


def load_datasets():
    means100, std100 = pipeline_montecarlo(5, 0.2, 1, 100, 1, 4, 10, 0.3, 100, 'load')
    means1000, std1000 = pipeline_montecarlo(5, 0.2, 1, 1000, 1, 4, 10, 0.3, 100, 'load')
    means10000, std10000 = pipeline_montecarlo(5, 0.2, 1, 10000, 1, 4, 10, 0.3, 100, 'load')
    return [means100,means1000,means10000],[std100,std1000,std10000]



def main():
    generate_and_save_datasets()
    x = [100, 1000, 10000]
    means, std = load_datasets()
    plot_metric(means, std, 'auc', 'Population (pop)', 'population', x)
    plot_metric(means, std, 'tpr', 'Population (pop)', 'population', x)


if __name__ == "__main__":
    main()
    print("done")







