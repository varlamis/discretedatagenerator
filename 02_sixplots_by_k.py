from sklearn import svm
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import VotingClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.tree import DecisionTreeClassifier

from utils import *

def pipeline_montecarlo(k,ratio_side,stdev,population,pos,neg,features, test_ratio,mc_iter, action):
    if action=='load':
        df=pd.read_csv('datasets/exp2_k='+str(k)+'_pop='+str(population)+'_feat='+str(features)+'.csv',sep=',', encoding='utf-8')
        df=df.drop(df.columns[0], axis = 1)
    elif action=='save':
        df=generate_dataset_parametric(k,ratio_side,stdev,population,pos,neg,features)
        df.to_csv('datasets/exp2_k='+str(k)+'_pop='+str(population)+'_feat='+str(features)+'.csv', sep=',', encoding='utf-8')
        return

    # prepare models
    models,names,colors = load_models_names_colors()

    r=evaluate_algorithms_montecarlo(df,models,test_ratio,mc_iter)
    means=np.mean(r,axis = 0)
    std=np.std(r,axis = 0)
    return means,std


def evaluate_algorithms_montecarlo(df,algos,test_ratio,iterations):
    allresults=[]
    for iter in np.arange(iterations):
        results=evaluate_train_test_split(df,algos,test_ratio)
        allresults.append(results)
    return np.array(allresults)

def generate_and_save_datasets():
    pipeline_montecarlo(3,0.2,1,1000,1,4,10,0.3,100,'save')
    pipeline_montecarlo(5,0.2,1,1000,1,4,10,0.3,100,'save')
    pipeline_montecarlo(7,0.2,1,1000,1,4,10,0.3,100,'save')
    pipeline_montecarlo(9,0.2,1,1000,1,4,10,0.3,100,'save')
    pipeline_montecarlo(11,0.2,1,1000,1,4,10,0.3,100,'save')


def load_datasets():
    means3,std3 = pipeline_montecarlo(3, 0.2, 1, 1000, 1, 4, 10, 0.3, 100, 'load')
    means5,std5=pipeline_montecarlo(5,0.2,1,1000,1,4,10,0.3,100,'load')
    means7,std7=pipeline_montecarlo(7,0.2,1,1000,1,4,10,0.3,100,'load')
    means9,std9=pipeline_montecarlo(9,0.2,1,1000,1,4,10,0.3,100,'load')
    means11,std11=pipeline_montecarlo(11,0.2,1,1000,1,4,10,0.3,100,'load')
    return [means3,means5,means7,means9,means11],[std3,std5,std7,std9,std11]



def main():
    generate_and_save_datasets()
    x = [3, 5, 7, 9, 11]
    means, std = load_datasets()
    plot_metric(means, std, 'auc', 'Partitions (k)', 'partitions',x)
    plot_metric(means, std, 'tpr', 'Partitions (k)', 'partitions', x)


if __name__ == "__main__":
    main()
    print("done")
